//
//  main.swift
//  puissance4
//
//  Created by etudiant on 17/01/2023.
//

import Foundation
import model

//let board = Board(withGrid: [[nil,nil,nil],[1,nil,1],[2,1,2]])
//if var b = board {
//    print(b.description)
//
//    b.insertPiece(id: 1, column: 1)
//    print(b.description)
//
//    b.insertPiece(id: 2, column: 1)
//    print(b.description)
//
//    b.removePiece(row: 1, column: 1)
//    print(b.description)

let lecteur = Lecteur()
let afficheur = Afficheur()

var b = Board()
if var board = b {
    
    var players = Array<Player>()
    
    for i in 0..<2 {
        var name: String = ""
        afficheur.afficherLigne(message: "Nom du joueur \(i + 1) ('Joueur \(i + 1)' par défaut): ")
        name = lecteur.lireLigne()!
        if name == nil || name == "" {
            name = "Joueur \(i + 1)"
        }
        
        var type = 0
        while (type != 1 && type != 2){
            afficheur.afficherLigne(message: "Quel est le type de \(name) ? \n\t1. Human\n\t2. IA")
            type = lecteur.lireInt()
        }
        
        if type == 1 {
            players.append(Human(named: name, readOn: lecteur))
        } else {
            players.append(IA(named: name, playedOn: &board))
        }
    }
    
    let rule = ClassicRules(withBoard: &board)
    
    var game = Game(withBoard: &board, playedBy: players, withRules: rule!, writeOn: afficheur)
    
    var winner : Player?
    while winner == nil && !rule!.isGameOver(board: &board) {
        winner = game.tour()
    }
}
