# Puis sans ce quatre

Application de puissance 4 en terminal développée en Swift.

## Fonctionnalités

- Jouer avec un ami
- Jouer contre un ordinateur
- Faire jouer deux ordinateurs l'un contre l'autre.

## Exécution

- Télécharger xcode [ici](https://developer.apple.com/xcode/)
- Ouvrir le *workspace* ``puissance4.xcworkspace``
- Cliquer sur le bouton d'exécution en haut à gauche (attention à bien exécuter le projet ``puissance 4``).

## Diagramme de classe

![Diagramme de classe](documentation/class%20diagram.svg "Diagramme de classe")