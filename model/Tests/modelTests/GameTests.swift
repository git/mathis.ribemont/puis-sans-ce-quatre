import XCTest
@testable import model

public class GameTests : XCTestCase {
    func test_constructor() {
        var board = Board()!
        let player1 = IA(named: "Lya", playedOn: &board)
        let player2 = IA(named: "Charle", playedOn: &board)
        let rule = ClassicRules(withBoard: &board)!
        let afficheur = Afficheur()
        
        XCTAssertNotNil(Game(withBoard: &board, playedBy: [player1, player2], withRules: rule, writeOn: afficheur))
    }
    
    func test_joueurSuivant() {
        var board = Board()!
        let player1 = IA(named: "Lya", playedOn: &board)
        let player2 = IA(named: "Charle", playedOn: &board)
        let rule = ClassicRules(withBoard: &board)!
        let afficheur = Afficheur()
        var game = Game(withBoard: &board, playedBy: [player1, player2], withRules: rule, writeOn: afficheur)
        
        XCTAssertEqual(0, game.numero)
        
        game.joueurSuivant()
        XCTAssertEqual(1, game.numero)
    }
}
