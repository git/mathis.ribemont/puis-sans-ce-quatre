import XCTest
@testable import model

final class IATests: XCTestCase {
    func test_ia() {
        let strength = 1000
        var board = Board()!
        let name = "Lya"
        
        let ia = IA(named: name, playedOn: &board)
        XCTAssertEqual(name, ia.name)
        
        for _ in 0..<strength {
            let n = ia.playInColumn()
            XCTAssertTrue(n >= 0)
            XCTAssertTrue(n < board.nbColumns)
        }
    }
}
