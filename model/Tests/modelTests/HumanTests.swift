import XCTest
@testable import model

final class HumanTests: XCTestCase {
    func test_construct() {
        let name = "charle"
        
        let player = Human(named: name, readOn: Lecteur())
        XCTAssertEqual(name, player.name)
    }
}
