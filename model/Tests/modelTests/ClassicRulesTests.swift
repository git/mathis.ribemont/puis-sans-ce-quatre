import XCTest
@testable import model

final class ClassicRulesTests: XCTestCase {
    public func test_execute(){
        let id = Int.random(in: 1...2)
        var array = newClassicGrid()
        
        XCTAssertNil(execute(column: 0, grid: array))
        
        array[3][3] = id
        XCTAssertNil(execute(column: 3, grid: array))
        
        //ligne
        array[3][3] = id
        XCTAssertEqual(nil, execute(column: 3, grid: array))
        array[3][5] = id
        XCTAssertEqual(nil, execute(column: 5, grid: array))
        array[3][6] = id
        XCTAssertEqual(nil, execute(column: 6, grid: array))
        array[3][4] = id
        XCTAssertEqual(id, execute(column: 4, grid: array))
        
        
        XCTAssertNil(execute(column: 0, grid: array))
        XCTAssertNil(execute(column: 1, grid: array))
        XCTAssertNil(execute(column: 2, grid: array))
        array = newClassicGrid()
        
        
        //colonne
        array[2][3] = id
        XCTAssertEqual(nil, execute(column: 3, grid: array))
        array[5][3] = id
        XCTAssertEqual(nil, execute(column: 3, grid: array))
        array[4][3] = id
        XCTAssertEqual(nil, execute(column: 3, grid: array))
        array[3][3] = id
        XCTAssertEqual(id, execute(column: 3, grid: array))
        
        XCTAssertNil(execute(column: 0, grid: array))
        XCTAssertNil(execute(column: 1, grid: array))
        XCTAssertNil(execute(column: 2, grid: array))
        XCTAssertNil(execute(column: 4, grid: array))
        XCTAssertNil(execute(column: 5, grid: array))
        XCTAssertNil(execute(column: 6, grid: array))
        array = newClassicGrid()
        
        
        
        // Diagonale NOSE gauche
        array[5][5] = id
        XCTAssertEqual(nil, execute(column: 5, grid: array))
        array[4][4] = id
        XCTAssertEqual(nil, execute(column: 4, grid: array))
        array[3][3] = id
        XCTAssertEqual(nil, execute(column: 3, grid: array))
        array[2][2] = id
        XCTAssertEqual(id, execute(column: 2, grid: array))
        
        XCTAssertNil(execute(column: 0, grid: array))
        XCTAssertNil(execute(column: 1, grid: array))
        XCTAssertNil(execute(column: 6, grid: array))
        array = newClassicGrid()
        
        // Diagonale NOSE gauche
        array[4][4] = id
        XCTAssertEqual(nil, execute(column: 4, grid: array))
        array[3][3] = id
        XCTAssertEqual(nil, execute(column: 3, grid: array))
        array[2][2] = id
        XCTAssertEqual(nil, execute(column: 2, grid: array))
        array[5][5] = id
        XCTAssertEqual(id, execute(column: 5, grid: array))
        
        XCTAssertNil(execute(column: 0, grid: array))
        XCTAssertNil(execute(column: 1, grid: array))
        XCTAssertNil(execute(column: 6, grid: array))
        array = newClassicGrid()
        
        
        // Diagonale SONE gauche
        array[5][2] = id
        XCTAssertEqual(nil, execute(column: 2, grid: array))
        array[3][4] = id
        XCTAssertEqual(nil, execute(column: 4, grid: array))
        array[4][3] = id
        XCTAssertEqual(nil, execute(column: 3, grid: array))
        array[2][5] = id
        XCTAssertEqual(id, execute(column: 5, grid: array))
        
        XCTAssertNil(execute(column: 0, grid: array))
        XCTAssertNil(execute(column: 1, grid: array))
        XCTAssertNil(execute(column: 6, grid: array))
        array = newClassicGrid()
        
        // Diagonale SONE droite
        array[2][5] = id
        XCTAssertEqual(nil, execute(column: 5, grid: array))
        array[3][4] = id
        XCTAssertEqual(nil, execute(column: 4, grid: array))
        array[4][3] = id
        XCTAssertEqual(nil, execute(column: 3, grid: array))
        array[5][2] = id
        XCTAssertEqual(id, execute(column: 2, grid: array))
        
        XCTAssertNil(execute(column: 0, grid: array))
        XCTAssertNil(execute(column: 1, grid: array))
        XCTAssertNil(execute(column: 6, grid: array))
        array = newClassicGrid()
        
    }
    
    private func execute(column: Int, grid: [[Int?]]) -> Int?{
        var board = Board(withGrid: grid)!
        let rule = ClassicRules(withBoard: &board)!
        return rule.execute(column: column, board: &board)
    }
    
    private func newClassicGrid() -> [[Int?]] {
        return Array(repeating: Array(repeating: nil, count: 7), count: 6)
    }
}
