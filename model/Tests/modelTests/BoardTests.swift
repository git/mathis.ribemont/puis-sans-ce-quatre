import XCTest
@testable import model

final class boardTests: XCTestCase {
    func test_constructor() throws {
        var nbR = -8
        var nbC = 10
        
        var b = Board(nbR: nbR, nbC: nbC)
        
        XCTAssertNil(b?.grid)
        
        nbR = 4
        nbC = -10
        b = Board(nbR: nbR, nbC: nbC)
        XCTAssertNil(b?.grid)
        

        nbR = 4
        nbC = 10
        b = Board(nbR: nbR, nbC: nbC)
        XCTAssertNotNil(b?.grid)
        XCTAssertEqual(nbR, b?.nbRows)
        XCTAssertEqual(nbC, b?.nbColumns)
    }
    
    func test_manipulatePiece() {
        var board = Board(nbR: 4, nbC: 4)
        
        board?.insertPiece(id: 1, column: 2)
        XCTAssertEqual(1, board?.grid[3][2])
        
//        XCTAssertEqual(BoardResult.ok, board?.insertPiece(id: 1, column: 2))
//        XCTAssertEqual(BoardResult.failed(.columnFull), board?.insertPiece(id: 1, column: 2))
        
        board?.removePiece(row: 3, column: 2)
        XCTAssertNil(board?.grid[3][2])
    }
    
    func test_cleanBoard(){
        var board = Board(withGrid: [[1,0,1],[0,1,0],[1,0,1]])
        
        if var b = board {
            
        XCTAssertNotNil(b.grid[0][0])
        
        b.clearGrid()
        
        
            for r in 0..<b.nbRows {
                for c in 0..<b.nbColumns {
                    XCTAssertNil(b.grid[r][c])
                }
            }
        }
    }
    
    func test_isFull(){
        var board = Board()!
        XCTAssertFalse(board.isFull())
        for i in 0..<board.nbColumns {
            for j in 0..<board.nbRows {
                board.insertPiece(id: 1, column: i)
                if i == board.nbColumns - 1 && j == board.nbRows - 1 {
                    XCTAssertTrue(board.isFull())
                } else {
                    XCTAssertFalse(board.isFull())
                }
            }
        }
    }
    
    func test_description() {
        let board = Board()!
        XCTAssertNotNil(board.description)
    }
}
