//
//  File.swift
//  
//
//  Created by etudiant on 01/02/2023.
//

import Foundation

public class ClassicRules : Rule {
    private let nbColMin = 4
    private let nbRowMin = 4
    private var isOver = false
    
    public init?(withBoard board: inout Board){
        guard board.nbColumns >= nbColMin && board.nbRows >= nbRowMin else {
            return nil
        }
    }
    
    public func isGameOver(board: inout Board) -> Bool {
        return board.isFull()
    }
    
    public func execute(column: Int, board: inout Board) -> Int? {
        let grid = board.grid
        let result = getLastPiece(column: column, board: &board)
        let y = result.y
        let id = result.id
        if id == 0 { return nil }
        
        var winCoord = [(x: Int, y: Int)]()
        
        var score = 0
        
        //bornes
        var minX = column - 3
        if minX < 0 {
            minX = 0
        }
        var maxX = column + 3
        if maxX >= board.nbColumns {
            maxX = board.nbColumns - 1
        }
        var minY = y - 3
        if minY < 0 {
            minY = 0
        }
        var maxY = y + 3
        if maxY >= board.nbRows {
            maxY = board.nbRows - 1
        }
        
        //ligne
        for i in minX...maxX {
            if grid[y][i] == id {
                score += 1
                winCoord += [(x: i, y: y)]
                if score >= 4 {
                    board.submitWinCoord(winCoord)
                    return id
                }
            } else {
                score = 0
                winCoord = [(x: Int, y: Int)]()
            }
        }
        winCoord = [(x: Int, y: Int)]()
        
        //colonne
        score = 0
        for i in minY...maxY {
            if grid[i][column] == id {
                score += 1
                winCoord += [(x: column, y: i)]
                if score >= 4 {
                    board.submitWinCoord(winCoord)
                    return id
                }
            } else {
                score = 0
                winCoord = [(x: Int, y: Int)]()
            }
        }
        winCoord = [(x: Int, y: Int)]()
        
        
        //diagonale NO -> SE
        score = 0
        var minOffsetNOSE = -3
        if column + minOffsetNOSE < 0 {
            minOffsetNOSE = 0 - column
        }
        if y + minOffsetNOSE < 0 {
            minOffsetNOSE = 0 - y
        }
        
        var maxOffsetNOSE = 3
        if column + maxOffsetNOSE >= board.nbColumns {
            maxOffsetNOSE = board.nbColumns - column - 1
        }
        if y + maxOffsetNOSE >= board.nbRows {
            maxOffsetNOSE = board.nbRows - y - 1
        }
        
        for i in minOffsetNOSE...maxOffsetNOSE {
            //print("\(y+i),\(column+i)\n")
            if grid[y+i][column+i] == id {
                score += 1
                winCoord += [(x: column+i, y: y+i)]
                if score >= 4 {
                    board.submitWinCoord(winCoord)
                    return id
                }
            } else {
                score = 0
                winCoord = [(x: Int, y: Int)]()
            }
        }
        winCoord = [(x: Int, y: Int)]()
        
        
        //diagonale SO -> NE
        score = 0
        var c = column + y
        var minOffsetSONE = -3
        
        if column + minOffsetSONE < 0 {
            minOffsetSONE = 0 - column
        }
        if y - minOffsetSONE >= board.nbRows {
            minOffsetSONE = y - (board.nbRows - 1)
        }
        
        var maxOffsetSONE = 3
        
        if column + maxOffsetSONE >= board.nbColumns {
            maxOffsetSONE = board.nbColumns - 1 - column
        }
        if y - maxOffsetSONE < 0 {
            maxOffsetSONE = y
        }
        //print("min: \(minOffsetSONE) max: \(maxOffsetSONE)")
        for i in minOffsetSONE...maxOffsetSONE {
            //print("\(column+i),\(y-i)")
            if grid[y-i][column+i] == id {
                score += 1
                winCoord += [(x: column+i, y: y-i)]
                if score >= 4 {
                    board.submitWinCoord(winCoord)
                    return id
                }
            } else {
                score = 0
                winCoord = [(x: Int, y: Int)]()
            }
        }
        return nil
    }
    
    private func getLastPiece(column: Int, board: inout Board) -> (y: Int, id: Int) {
        let grid = board.grid
        var y = 0;
        var id = 0
        for i in 0..<board.nbRows {
            if let idCase = grid[i][column] {
                y = i
                id = idCase
                break
            }
        }
        
        return (y, id);
    }
}
