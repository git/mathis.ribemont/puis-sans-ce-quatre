//
//  File.swift
//  
//
//  Created by etudiant on 31/01/2023.
//

import Foundation

public class Lecteur : ILecteur  {
    public init() {
        
    }
    
    public func lireInt() -> Int{
        if let value = readLine() {
            return Int(value) ?? -1
        } else {
            return -1
        }
    }
    
    public func lireLigne() -> String? {
        return readLine()
    }
}
