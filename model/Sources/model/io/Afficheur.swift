//
//  File.swift
//  
//
//  Created by etudiant on 31/01/2023.
//

import Foundation

public class Afficheur : IAfficheur {
    public init() {
        
    }
    
    public func afficherLigne(message msg: String){
        print(msg)
    }
}
