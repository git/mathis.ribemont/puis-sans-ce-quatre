//
//  File.swift
//  
//
//  Created by etudiant on 25/01/2023.
//

import Foundation

public class Human : Player {
    public var name: String
    private var lecteur: ILecteur
    
    // au lieu du protocol Lecteur, prendre une fonction qui sera appelé à chaque fois si on ne veux qu'une seul méthode (c'est une closure, si on la stock, on doit mettre @escaping en paramètre). On peut stocker une méthode optionnel
    public init(named name: String, readOn lecteur: ILecteur){
        self.name = name
        self.lecteur = lecteur
    }
    
    public func playInColumn() -> Int {
        lecteur.lireInt()
    }
    
    
}
