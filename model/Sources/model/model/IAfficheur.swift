//
//  File.swift
//  
//
//  Created by etudiant on 10/02/2023.
//

import Foundation

public protocol IAfficheur {
    func afficherLigne(message msg: String)
}
