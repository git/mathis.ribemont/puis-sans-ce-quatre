//
//  File.swift
//  
//
//  Created by etudiant on 19/01/2023.
//

import Foundation

public protocol Player {
    var name: String { get }
    
    func playInColumn() -> Int
}
