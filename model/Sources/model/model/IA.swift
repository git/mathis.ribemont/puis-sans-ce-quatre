//
//  File.swift
//  
//
//  Created by etudiant on 25/01/2023.
//

import Foundation

public class IA: Player {
    public var name: String
    private var board: Board
    
    public init(named name: String, playedOn board: inout Board){
        self.name = name
        self.board = board
    }
    
    public func playInColumn() -> Int {
        return Int.random(in: 0..<(board.nbColumns))
    }
    
    
}
