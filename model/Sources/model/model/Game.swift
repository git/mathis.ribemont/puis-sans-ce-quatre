//
//  File.swift
//  
//
//  Created by etudiant on 19/01/2023.
//

import Foundation

public struct Game {
    private var board: Board
    private var players: [Player]
    private(set) var numero: Int = 0
    private var afficheur: IAfficheur
    private var rule: Rule
    
    public init(withBoard board: inout Board, playedBy players: [Player], withRules rule: Rule, writeOn afficheur: IAfficheur) {
        self.board = board
        self.players = players
        self.afficheur = afficheur
        self.rule = rule
    }
    
    public mutating func tour() -> Player?{
        let player = players[numero]
        var result = BoardResult.unknown
        var choice = 0
        while(result != BoardResult.ok){
            afficheur.afficherLigne(message: "\n\(player.name), dans quelle colonne voulez-vous insérer un jeton ?")
            choice = player.playInColumn()
            afficheur.afficherLigne(message: "La colonne choisie est \(choice).")
            result = board.insertPiece(id: numero + 1, column: choice)
            switch result {
            case BoardResult.failed(FailedReason.unknown):
                afficheur.afficherLigne(message: "Vous ne pouvez pas placer de pièce ici.")
                break
            case BoardResult.failed(FailedReason.columnFull):
                afficheur.afficherLigne(message: "Vous ne pouvez pas placer de pièce ici.")
                break
            case BoardResult.failed(FailedReason.boardFull):
                afficheur.afficherLigne(message: "Vous ne pouvez pas placer de pièce ici.")
                break
            case BoardResult.failed(FailedReason.outOfBound):
                afficheur.afficherLigne(message: "Vous ne pouvez pas placer de pièce ici.")
                break
            default: break
            }
        }
        let winnerId = rule.execute(column: choice, board: &board)
        afficheur.afficherLigne(message: board.description)
        if winnerId != nil {
            let index = winnerId! - 1
            afficheur.afficherLigne(message: "\(players[index].name) a gagné !")
            return players[winnerId! - 1]
        }
        joueurSuivant()
        return nil
    }
    
    public mutating func joueurSuivant() {
        numero = (numero + 1) % players.count
    }
}
