//
//  File.swift
//  
//
//  Created by etudiant on 17/01/2023.
//

import Foundation

public struct Board: CustomStringConvertible {
    var grid: [[Int?]];
    let nbRows: Int;
    let nbColumns: Int;
    private var winCoord: Array<(x: Int, y: Int)>?
    private static let descriptionMapper: [Int?:String] = [nil:"⚫", 1:"🔴", 2:"🟡"];
    private static let winnerDescriptionMapper = "🟢"
    
    public init?(nbR: Int = 6, nbC: Int = 7) {
        if nbR <= 0 || nbC <= 0 {
            return nil
        }
        self.nbRows = nbR
        self.nbColumns = nbC
        self.grid = Array(repeating: Array(repeating: nil, count: nbC), count: nbR)
    }
    
    public init?(withGrid grid : [[Int?]]){
        let sizes = grid.map{ return $0.count }
        
        let result = sizes.allSatisfy {
            $0 == grid[0].count
        }
        
        guard result else {
            return nil
        }
        
        nbRows = grid.count
        nbColumns = grid[0].count
        self.grid=grid
    }
    
    
    public var description: String {
        var string = String()
        
        for row in 0..<nbRows {
            string.append("|")
            for column in 0..<nbColumns {
                if winCoord != nil && winCoord!.contains(where: { $0.x == column && $0.y == row } ) {
                    string.append("\(Board.winnerDescriptionMapper)")
                } else {
                    let cell = grid[row][column]
                    string.append("\(String(describing: Board.descriptionMapper[cell] ?? " "))")
                }
                string.append("|")
            }
            string.append("\n")
        }
        
        /*string.append("|")
        for _ in 0...grid.count{
            string.append("--|")
        }
        string.append("\n")*/
        
        string.append(" ")
        for i in 0...grid.count{
            string.append(" \(i) ")
        }
        
        return string
    }
    
    public func isFull() -> Bool {
        for column in 0..<nbColumns{
            if !isColumnFull(column: column) {
                return false
            }
        }
        
        return true
    }
    
    private func isColumnFull(column: Int) -> Bool{
        if grid[0][column] == nil {
            return false
        }
        return true
    }
    
    private mutating func insertPiece(id:Int, row:Int, column:Int) -> BoardResult {
        guard row >= 0 && row < nbRows && column >= 0 && column < nbColumns else {
            return .failed(.outOfBound)
        }
        
        guard grid[row][column] == nil else {
            return .failed(.unknown)
        }
        
        grid[row][column] = id
        return .ok
    }
    
    public mutating func insertPiece(id:Int, column:Int) -> BoardResult {
        var result = BoardResult.unknown
        for row in (0..<nbRows ).reversed() {
            if row < 0 {
                return .failed(.columnFull)
            }
            result = insertPiece(id: id, row: row, column: column)
            switch result {
            case .ok:
                return .ok
            default:
                break
            }
        }
        return .failed(.unknown)
    }
    
    public mutating func removePiece(row:Int, column:Int) -> BoardResult {
        guard row >= 0 && row < nbRows && column >= 0 && column < nbColumns else {
            return .failed(.outOfBound)
        }
        
        grid[row][column] = nil
        return .ok
    }
    
    public mutating func clearGrid(){
        for r in 0..<nbRows {
            for c in 0..<nbColumns {
                grid[r][c] = nil
            }
        }
    }
    
    public mutating func submitWinCoord(_ winCoord: [(x: Int, y: Int)]) {
        self.winCoord = winCoord
    }
}
