//
//  File.swift
//  
//
//  Created by etudiant on 31/01/2023.
//

import Foundation

// en static, le nombre de ligne, de colonne, et de case à aligner
// méthode createBoard, ou alors permettre de vérifier si la board est valid
// isValid(column) , IsGameOver(column),

// Pour avoir une collection de règles, créer une règle qui étend ce protocol, et qui contiendra une liste de rules, qui elles vont faire la logique. Ca permettra de juger les priorités des règles.
public protocol Rule {
    func execute(column: Int, board: inout Board) -> Int?
    func isGameOver(board: inout Board) -> Bool
}
