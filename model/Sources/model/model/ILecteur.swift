//
//  File.swift
//  
//
//  Created by etudiant on 10/02/2023.
//

import Foundation

public protocol ILecteur {
    func lireInt() -> Int
    func lireLigne() -> String?
}
